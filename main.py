"""! @brief Network Analyzer that is capable of capturing network traffic,
 perform host discovery, perform port scanning, perform ARP poisoning and DNS spoofing attacks

@section libraries_main Libraries/Modules
- PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
  - Access to QApplication
- gui_main_window (local)
  - Access to all classes under gui_main_window class

@section author_main Author(s)
- Created by Erlandas Bacauskas on 2021/2022

Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
"""

##
# @mainpage Network Analyzer
#
# @section description_main Description
# Network Analyzer that is capable of capturing network traffic,
# perform host discovery, perform port scanning, perform ARP poisoning and DNS spoofing attacks.
#
# @section notes_main Notes
# - Add some notes as I go
#
# Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
# To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/

##
# @file main.py
#
# @brief Starting point for a program
#
# @section description_main Description
# Starting point for Network Analyzer
#
# @section libraries_main Libraries/Modules
# - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
#   - Access to QApplication
# - gui_main_window (local)
#   - Access to all classes under gui_main_window class
#
# @section notes_main Notes
# - Starting point for Network Analyzer
#
# @section todo_main TODO
# - Implement DNS poisoning for multiple OS's
#
# @section author_main Author(s)
# - Created by Erlandas Bacauskas on 2021/2022
#
# Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
# To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/

# Imports

from PyQt5.QtWidgets import QApplication
from gui_main_window import MainWindow
from scapy.all import *

import socket


if __name__ == '__main__':
    """! Main Network Analyzer entry."""
    # Local Constants
    ## Initializes main window
    main_window = QApplication(sys.argv)
    ## Calls main window (executes program)
    window = MainWindow()
    sys.exit(main_window.exec_())
