"""!
    @brief Defines a Sniffer class
"""

import scapy.interfaces
from scapy.all import *
from PyQt5.QtWidgets import (
    QWidget,
    QGridLayout,
    QPushButton,
    QComboBox,
    QLabel,
    QTableWidget,
    QPlainTextEdit,
    QTableWidgetItem,
    QFileDialog, QCheckBox
)
from db_controls import Controller
import socket
from collections import Counter
from prettytable import PrettyTable
import plotly
import matplotlib.pyplot as plt

## Stops sniffing when is set to True, by default it is set to False
sniff_break = False


class Sniffer(QWidget):
    """!
        @brief Defines a Sniffer class

        @section description_sniffer Description
        The Sniffer class is responsible for all components that is capable to capture network traffic
        and displaying result to user. Within application data can be filtered by type.
        Capture files can be saved and opened within application.

        @section libraries_gui_sniffer Libraries/Modules
        - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
          - Access to QWidget
          - Access to QGridLayout
          - Access to QPushButton
          - Access to QComboBox
          - Access to QLabel
          - Access to QTableWidget
          - Access to QPlainTextEdit
          - Access to QTableWidgetItem
          - Access to QFileDialog
          - Access to QCheckBox
        - Scapy https://scapy.readthedocs.io/en/latest/
          - Access to scapy functionality
        - db_controls
          - Access to Controller class

        @section author_gui_sniffer Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
    """

    def __init__(self, tabs):
        """!
            The Sniffer class initializer.
            @return Tab for a Sniffer initialized with components
        """
        super().__init__()

        ## Creates tab that will be returned
        self.sniffer_tab = QWidget()
        ## Creates layout that will be used for a tab
        self.layout = QGridLayout()

        # Create table
        ## Creates table for network capture results
        self.table = QTableWidget()
        self.table.setColumnCount(5)
        self.table.setRowCount(0)
        self.table.setHorizontalHeaderLabels(["Source", "Destination", "Protocol", "Type", "Summary"])
        self.table.clicked.connect(self.table_row_item_selected)

        # Setup Main buttons (Run, Stop
        ## Sets offset for buttons
        self.btn_offset = 100
        ## Sets run button text
        self.btn_run_text = "Run"
        ## Initializes run button
        self.btn_run = QPushButton(self.btn_run_text)
        self.btn_run.setMaximumWidth(
            self.btn_run.fontMetrics().boundingRect(self.btn_run_text).width() + self.btn_offset)
        self.btn_run.clicked.connect(self.start_sniff)
        self.btn_run.setAutoDefault(True)

        ## Sets stop button text
        self.btn_stop_text = "Stop"
        ## Initializes stop button
        self.btn_stop = QPushButton(self.btn_stop_text)
        self.btn_stop.setMaximumWidth(
            self.btn_stop.fontMetrics().boundingRect(self.btn_stop_text).width() + self.btn_offset)
        self.btn_stop.clicked.connect(self.stop_sniff)
        self.btn_stop.setAutoDefault(True)

        ## Sets save button text
        self.btn_save_text = "Save"
        ## Initializes save button
        self.btn_save = QPushButton(self.btn_save_text)
        self.btn_save.setMaximumWidth(
            self.btn_save.fontMetrics().boundingRect(self.btn_save_text).width() + self.btn_offset)
        self.btn_save.clicked.connect(self.save_pcap)
        self.btn_save.setAutoDefault(True)

        ## Sets open button text
        self.btn_open_text = "Open"
        ## Initializes open button
        self.btn_open = QPushButton(self.btn_open_text)
        self.btn_open.setMaximumWidth(
            self.btn_open.fontMetrics().boundingRect(self.btn_open_text).width() + self.btn_offset)
        self.btn_open.clicked.connect(self.open_pcap)
        self.btn_open.setAutoDefault(True)

        # DropdownBox for interface list
        ## Initializes dropdown combo box for interfaces
        self.dropdown_interfaces = QComboBox()
        ## Gets interfaces from underlying operating system
        self.interfaces = scapy.interfaces.get_if_list()
        self.dropdown_interfaces.addItem("")
        for interface in self.interfaces:
            self.dropdown_interfaces.addItem(interface)
        ## Current interface selection default is None
        self.dropdown_if_selection = ""
        self.dropdown_interfaces.activated.connect(self.interface_selected)

        # Filter input DO BUTTON to get text "Set Filter"
        ## Initializes dropdown combo box for filters
        self.filter_input_combobox = QComboBox()
        self.filter_input_combobox.setMaximumWidth(self.btn_run.width()*2.5)
        self.filter_input_combobox.setEditable(True)
        self.populate_filter_options()

        ## Sets filter button text
        btn_set_filter_text = "Set Filter"
        ## Initializes set filter button
        btn_set_filter = QPushButton(btn_set_filter_text)
        btn_set_filter.setMaximumWidth(
            self.btn_run.fontMetrics().boundingRect(btn_set_filter_text).width() + self.btn_offset)
        btn_set_filter.clicked.connect(self.set_filter)

        # Clear table button
        ## Sets clear button text
        self.btn_clear_text = "Clear"
        ## Initializes clear button
        self.btn_clear = QPushButton(self.btn_clear_text)
        self.btn_clear.setMaximumWidth(
            self.btn_clear.fontMetrics().boundingRect(self.btn_clear_text).width() + self.btn_offset)
        self.btn_clear.clicked.connect(self.clear_table)
        self.btn_clear.setAutoDefault(True)

        # Clear table button
        ## Sets clear button text
        self.btn_graph_text = "Bar Chart"
        ## Initializes clear button
        self.btn_graph = QPushButton(self.btn_graph_text)
        self.btn_graph.setMaximumWidth(
            self.btn_graph.fontMetrics().boundingRect(self.btn_graph_text).width() + self.btn_offset)
        self.btn_graph.clicked.connect(self.plot_bar_chart)
        self.btn_graph.setAutoDefault(True)

        # Clear table button
        ## Sets clear button text
        self.btn_pie_text = "Pie Chart"
        ## Initializes clear button
        self.btn_pie = QPushButton(self.btn_pie_text)
        self.btn_pie.setMaximumWidth(
            self.btn_pie.fontMetrics().boundingRect(self.btn_pie_text).width() + self.btn_offset)
        self.btn_pie.clicked.connect(self.plot_pie_chart)
        self.btn_pie.setAutoDefault(True)

        # Text area to print raw data packet
        ## Initializes field for raw text
        self.raw_field = QPlainTextEdit()
        self.raw_field.setReadOnly(True)

        # Text area to print feedback for a user
        ## Initializes field for feedback text
        self.feedback_field = QPlainTextEdit()
        self.feedback_field.setReadOnly(True)

        # text field details of packet
        ## Initializes field for packets details
        self.details_field = QPlainTextEdit()
        self.details_field.setReadOnly(True)

        # Packet label (Visual aid how many packets are captured)
        ## Initializes label for amount of packets captured
        self.progress_label = QLabel("Packets: 0")

        ## Checkbox that that allows host name mapping
        self.host_name_checkbox = QCheckBox("Host Names")

        # Position components
        self.layout.addWidget(self.btn_run, 0, 0, 1, 1)
        self.layout.addWidget(self.btn_stop, 0, 1, 1, 1)
        self.layout.addWidget(self.btn_save, 0, 2, 1, 1)
        self.layout.addWidget(self.btn_open, 0, 3, 1, 1)
        self.layout.addWidget(self.btn_clear, 0, 4, 1, 1)
        self.layout.addWidget(self.btn_graph, 0, 5, 1, 1)
        self.layout.addWidget(self.btn_pie, 0, 6, 1, 1)

        self.layout.addWidget(QLabel("          Interface"), 0, 7, 1, 1)
        self.layout.addWidget(self.dropdown_interfaces, 0, 8, 1, 1)
        self.layout.addWidget(QLabel("          Filter"), 0, 9, 1, 1)
        self.layout.addWidget(self.filter_input_combobox, 0, 10, 1, 1)
        self.layout.addWidget(btn_set_filter, 0, 11, 1, 1)

        self.layout.addWidget(self.host_name_checkbox, 0, 12, 1, 1)

        self.layout.addWidget(self.progress_label, 0, 13, 1, 2)
        self.layout.addWidget(self.table, 1, 0, 1, 9)
        self.layout.addWidget(self.feedback_field, 2, 0, 2, 4)
        self.layout.addWidget(self.raw_field, 2, 4, 2, 5)
        self.layout.addWidget(self.details_field, 1, 9, 3, 5)

        # Variables
        ## Initializes thread for a sniffer (sniff function runs in its own thread)
        self.sniffer_thread = None
        ## Initializes variable for captured results from sniff function
        self.sniffer_results = None
        ## Initializes filter options to None
        self.filter_option = ""
        ## Status of sniffer default is False
        self.is_running = False
        ## Initializes packets captired to None
        self.packets_count = ""
        ## Initializes boolean for a filter option if it is wrong default is False
        self.is_filter_wrong = False
        ## variable that stores source ips for statistics
        self.source_ips = []
        self.sniffer_tab.setLayout(self.layout)
        tabs.addTab(self.sniffer_tab, "Sniffer")

    def plot_pie_chart(self):
        """!
            Creates Pie chart from loaded data from captures
        """
        addresses = []
        slices = []

        counter = Counter()
        for ip in self.source_ips:
            counter[ip] += 1

        for ip, count in counter.most_common():
            addresses.append(ip)
            slices.append(count)

        plt.figure("Pie Chart")
        plt.pie(slices, labels=addresses, labeldistance=1000,
                startangle=90, shadow=False, explode=None,
                radius=1.5, autopct='%1.1f%%')

        plt.legend()
        plt.show()

    def plot_bar_chart(self):
        """!
            Creates Bar chart from loaded data from captures
        """
        left = []
        height = []
        tick_label = []

        counter = Counter()
        for ip in self.source_ips:
            counter[ip] += 1

        index = 1
        for ip, count in counter.most_common():
            tick_label.append(ip)
            height.append(count)
            left.append(index)
            index += 1

        plt.figure("Bar Chart")
        plt.bar(left, height, tick_label=tick_label,
                width=0.8, color=['blue', 'red'])

        plt.xlabel("IP's/Host Names")
        plt.ylabel('Occurrences')
        plt.title('Packet Statistics')
        plt.show()

    def statistics(self):
        """!
            Organizes captured data by frequency and presents to user in readable manner
        """
        counter = Counter()
        for ip in self.source_ips:
            counter[ip] += 1
        statistics_table = PrettyTable(["IP", "Count"])
        for ip, count in counter.most_common():
            statistics_table.add_row([ip, count])
        statistics_table.align["IP"] = "l"
        statistics_table.align["Count"] = "r"
        self.raw_field.setPlainText(str(statistics_table))

    def populate_filter_options(self):
        """!
            Populates gathered data from database in a filter options dropdown field.
            - Initializes controller
            - Retrieves data (if any)
            - Adds each data item gathered
        """
        db = Controller()
        data = db.select_filter_option_items()
        self.filter_input_combobox.addItem("")
        for a, value in data:
            self.filter_input_combobox.addItem(value)

    def update_filter_options_table(self):
        """!
            Inserts NOT present item to database (filter options)
            - Checks if item is NOT present within database (no duplicates allowed)
            - If present returns
            - If not present inserts item to database
            - Adds item to filter dropdown box
        """
        db = Controller()
        if db.is_filter_option_item_present(self.filter_input_combobox.currentText()) \
                or self.filter_input_combobox.currentText() == "":
            return

        db.insert_filter_option(self.filter_input_combobox.currentText())
        self.filter_input_combobox.addItem(self.filter_input_combobox.currentText())

    def get_host_name(self, ip):
        """!
            Function that resolves given IP address to a host name.

            @param ip  IP address to resolve
            @return Domain name or IP address if it couldn't resolve
        """
        try:
            host_name = str(socket.gethostbyaddr(ip)[0])
            return host_name
        except socket.error:
            return ip

    def start_sniff(self):
        """!
            Function that is activated when RUN button is pressed. Starts network capture.
            - Checks if network capture is not active
            - If it is active adds entry to feedback field and function terminates
            - if NOT active:
            - Clears table that holds captured packets if any
            - Takes current interface from interfaces dropdown
            - Sets running to True
            - Sets sniff break to false
            - Initializes sniffer thread
            - Starts sniffer thread
            - Checks if filter provided is valid
            - If NOT valid:
            - Boolean set that indicates that filter is wrong
            - Filter is discarded and network capture is run without filter option
            - Resets filter to None
            - Adds feedback to user indicating a problem
            - If filter is valid or None starts network capture
        """
        if self.is_running is not True:
            self.clear_table()

            interface = self.dropdown_interfaces.currentText()
            if interface == "":
                interface = "Default"

            self.is_running = True
            global sniff_break
            sniff_break = False
            self.sniffer_thread = threading.Thread(target=self.sniff_thread, args=(1,))
            self.sniffer_thread.start()

            if not self.is_filter_wrong:
                self.feedback_field.appendPlainText("[+] Sniffing started on '" + interface + "' interface")

            if self.is_filter_wrong:
                self.is_filter_wrong = False
                self.filter_option = ""
                self.filter_input_combobox.setCurrentIndex(0)
                self.add_feedback("Wrong filter option provided, check about tab for filter option examples.")
                self.add_feedback("Filter reset")
        else:
            self.feedback_field.appendPlainText("[+] Sniffing is running stop to run it again")

    def sniff_thread(self, a):
        """!
            Function that is run in a separate thread. Captures network packets.
            - Runs while sniff break is set to False
            - Assesses filter validity
            - If filter is invalid terminates function
            - If sniff break set to True:
            - Resets thread
            - Resets filter option to None
            - Terminates function
        """
        while True:
            if sniff_break:
                self.sniffer_thread = threading.Thread
                self.filter_option = ""
                break
            try:
                self.sniffer_results = sniff(iface=self.dropdown_if_selection,
                                             prn=self.add_table_entry,
                                             filter=self.filter_option,
                                             stop_filter=self.stop_sniffing)
            except scapy.error.Scapy_Exception as e:
                self.is_running = False
                self.is_filter_wrong = True
                break


    def stop_sniffing(self, a):
        """!
            Function that is used by Scapy sniff function. After each packet capture checks if sniffing still
            should be active.
        """
        if sniff_break:
            return True
        else:
            return False
        pass

    def stop_sniff(self):
        """!
            Function that stops network capture. Activated when stop button pressed.
            - Adds feedback
            - Resets filter options
            - Sets running to False
            - Sets sniff break to True
        """
        self.add_feedback("Sniffing stopped")
        self.filter_option = ""
        self.filter_input_combobox.setCurrentIndex(0)
        self.add_feedback("Filter reset")
        self.is_running = False
        self.filter_option = ""
        global sniff_break
        sniff_break = True
        self.statistics()

    def add_table_entry(self, pkt):
        """!
            Function than takes fields from the packet and adds entries to a table.
            - Assigns  packet type by analyzing numeric value of a packet
            - If IP packet takes needed fields for IP packet
            - Adds entry to table
            - Terminates function
            - Updates packet count
            - If ARP packet takes needed fields for ARP packet
            - Adds entry to table
            - Terminates function
            - Updates packet count
            - If IPv6 packet takes needed fields for IPv6 packet
            - Adds entry to table
            - Terminates function
            - Updates packet count

            @param pkt  Captured packet that is dissected
        """
        # Set type as it is present in each of the packets
        pkt_type = ""
        if pkt[0][0].type == 2048:
            pkt_type = "IPv4"
        elif pkt[0][0].type == 2054:
            pkt_type = "ARP"
        elif pkt[0][0].type == 34525:
            pkt_type = "IPv6"

        row_pos = self.table.rowCount()

        if "IP" in pkt:
            self.table.insertRow(row_pos)
            if self.host_name_checkbox.isChecked():
                self.source_ips.append(self.get_host_name(pkt[0][1].src))
                self.table.setItem(row_pos, 0, QTableWidgetItem(self.get_host_name(pkt[0][1].src)))
                self.table.setItem(row_pos, 1, QTableWidgetItem(self.get_host_name(pkt[0][1].dst)))
            else:
                self.source_ips.append(pkt[0][1].src)
                self.table.setItem(row_pos, 0, QTableWidgetItem(pkt[0][1].src))
                self.table.setItem(row_pos, 1, QTableWidgetItem(pkt[0][1].dst))

            protocol = ""
            if pkt[0][1].proto == 1:
                protocol = "ICMP"
            elif pkt[0][1].proto == 6:
                protocol = "TCP"
            elif pkt[0][1].proto == 17:
                protocol = "UDP"

            self.table.setItem(row_pos, 2, QTableWidgetItem(protocol))
            self.table.setItem(row_pos, 3, QTableWidgetItem(pkt_type))
            self.table.setItem(row_pos, 4, QTableWidgetItem(pkt.summary()))
        elif "ARP" in pkt:
            self.table.insertRow(row_pos)
            if self.host_name_checkbox.isChecked():
                self.table.setItem(row_pos, 0, QTableWidgetItem(self.get_host_name(pkt[0][1].psrc)))
                self.table.setItem(row_pos, 1, QTableWidgetItem(self.get_host_name(pkt[0][1].pdst)))
            else:
                self.table.setItem(row_pos, 0, QTableWidgetItem(pkt[0][1].psrc))
                self.table.setItem(row_pos, 1, QTableWidgetItem(pkt[0][1].pdst))
            #self.table.setItem(row_pos, 0, QTableWidgetItem(pkt[0][1].psrc))
            #self.table.setItem(row_pos, 1, QTableWidgetItem(pkt[0][1].pdst))
            self.table.setItem(row_pos, 2, QTableWidgetItem("ARP"))
            self.table.setItem(row_pos, 3, QTableWidgetItem(pkt_type))
            self.table.setItem(row_pos, 4, QTableWidgetItem(pkt.summary()))

        elif "IPv6" in pkt:
            self.table.insertRow(row_pos)

            ipv6_protocol = ""
            if pkt[0][1].nh == 1:
                ipv6_protocol = "ICMP"
            elif pkt[0][1].nh == 6:
                ipv6_protocol = "TCP"
            elif pkt[0][1].nh == 17:
                ipv6_protocol = "UDP"

            if self.host_name_checkbox.isChecked():
                self.table.setItem(row_pos, 0, QTableWidgetItem(self.get_host_name(pkt[0][1].src)))
                self.table.setItem(row_pos, 1, QTableWidgetItem(self.get_host_name(pkt[0][1].dst)))
            else:
                self.table.setItem(row_pos, 0, QTableWidgetItem(pkt[0][1].src))
                self.table.setItem(row_pos, 1, QTableWidgetItem(pkt[0][1].dst))
            #self.table.setItem(row_pos, 0, QTableWidgetItem(pkt[0][1].src))
            #self.table.setItem(row_pos, 1, QTableWidgetItem(pkt[0][1].dst))
            self.table.setItem(row_pos, 2, QTableWidgetItem(ipv6_protocol))
            self.table.setItem(row_pos, 3, QTableWidgetItem(pkt_type))
            self.table.setItem(row_pos, 4, QTableWidgetItem(pkt.summary()))

        self.packets_count = str(row_pos+1)
        self.progress_label.setText("Packets: " + self.packets_count)

    def save_pcap(self):
        """!
            Opens file dialog to select file to be saved OR creates new file with a capture (.pcap).
            - Checks if network capture is NOT active
            - If active:
            - Adds feedback
            - Terminates function
            - If NOT active
            - Opens file dialog to save file
        """
        if self.is_running is not True:
            if self.sniffer_results is not None:
                file_name, _ = QFileDialog.getSaveFileName(self,
                                                           "Save", "",
                                                           "Pcap Files (*.pcap)",
                                                           options=QFileDialog.DontUseNativeDialog)
                if file_name:
                    wrpcap(file_name, self.sniffer_results)
                    self.add_feedback("Saving sniff results ({})".format(file_name))
            else:
                self.add_feedback("There is no results to save")
        else:
            self.add_feedback("While sniffing is active SAVE is prohibited")

    def open_pcap(self):
        """!
            Opens file dialog to select file to be opened within application (.pcap).
            - Checks if network capture is NOT active
            - If active:
            - Adds feedback
            - Terminates function
            - If NOT active
            - Opens file dialog to select file
            - When file selected clears table entries
        """
        if self.is_running is not True:
            file_name, _ = QFileDialog.getOpenFileName(self,
                                                       "Open", "",
                                                       "Pcap Files (*.pcap)",
                                                       options=QFileDialog.DontUseNativeDialog)
            if file_name:
                self.clear_table()
                self.sniffer_results = sniff(offline=file_name, prn=self.add_table_entry)
        else:
            self.add_feedback("While sniffing is active OPEN is prohibited")

    def interface_selected(self):
        """!
            Sets selected interface (network card)
            - Displays feedback for user
        """
        interface = self.dropdown_interfaces.currentText()
        int_placeholder = ""
        if interface == "":
            int_placeholder = "Default"
        else:
            int_placeholder = self.dropdown_interfaces.currentText()
        self.dropdown_if_selection = interface
        self.add_feedback("Active interface selected '" + int_placeholder + "'")

    def table_row_item_selected(self):
        """!
            On a row selected within capture table, displays details of a packet selected within details field.
        """
        try:
            index = self.table.currentRow()
            self.add_feedback("Results selected from row " + str(index + 1))
            self.details_field.setPlainText(str(self.sniffer_results[index].show(dump=True)))
        except (IndexError, TypeError):
            self.add_feedback("Inspect packets when sniffing is stopped")

    def set_filter(self):
        """!
            Sets current filter option within filter field. Activated when set filter button is pressed.
            - Assigns filter value to variable
            - Adds feedback
            - Calls function to update database table with used filter option
        """
        self.filter_option = self.filter_input_combobox.currentText()
        self.add_feedback("Filter set to '" + self.filter_option + "'")
        self.update_filter_options_table()

    def clear_table(self):
        """!
            Clears table network capture table entries. Activated when clear button pressed.
            - If network capture is active:
            - Appends text to feedback field indicating of active network capture
            - Terminates function
            - If network capture is not active:
            - If table is empty terminates function
            - If NOT empty:
            - Erases details field sets to None
            - Appends feedback
            - Sets table row counts to 0 (erasing all entries)
            - Sets packet count to 0
            - Sets packet capture label to indicate that 0 packets captured
        """
        if self.is_running is not True:
            if self.table.rowCount() != 0:
                self.details_field.setPlainText("")
                self.add_feedback("Clearing out previous results")
                self.table.setRowCount(0)
                self.packets_count = str(0)
                self.progress_label.setText("Packets: " + self.packets_count)
                self.raw_field.clear()
                self.source_ips = []
        else:
            self.add_feedback("Stop to clear results")

    def add_feedback(self, feedback_text):
        """!
            Appends feedback to user of current activity

            @param feedback_text  String that will be displayed within feedback field
        """
        lock = threading.Lock()
        lock.acquire()
        try:
            self.feedback_field.appendPlainText("[+] " + feedback_text)
        finally:
            lock.release()

