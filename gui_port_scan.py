"""!
    @brief Defines a PortScan class
"""
# TODO find way to check if it is valid ip address and range

import scapy.sendrecv
from PyQt5.QtWidgets import (
    QWidget,
    QGridLayout,
    QPushButton,
    QLineEdit,
    QPlainTextEdit,
    QTableWidget,
    QTableWidgetItem,
    QComboBox
)
from scapy.all import *
from scapy.layers.inet import IP, ICMP, TCP
from db_controls import Controller


class PortScan(QWidget):
    """!
        @brief Defines a PortScan class

        @section description_port_scan Description
        The PortScan class is responsible for all components that is capable of scanning ports for a given host.

        @section libraries_port_scan Libraries/Modules
        - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
          - Access to QWidget
          - Access to QGridLayout
          - Access to QPushButton
          - Access to QComboBox
          - Access to QCheckBox,
          - Access to QPlainTextEdit
        - Threading https://docs.python.org/3/library/threading.html
        - Scapy https://scapy.readthedocs.io/en/latest/
          - Access to scapy functionality
        - db_controls
          - Access to Controller class

        @section author_port_scan Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
    """

    def __init__(self, tabs):
        """!
            The PortScan class initializer.
            @return Tab for port scan initialized with components
        """
        super().__init__()
        ## Creates tab that will be returned
        self.port_scan_tab = QWidget()

        # Components
        # Start button
        ## Sets offset for buttons
        self.btn_offset = 100
        ## Sets start button text
        self.btn_start_text = "Start"
        ## Initializes start button
        self.btn_start = QPushButton(self.btn_start_text)
        self.btn_start.setMaximumWidth(
            self.btn_start.fontMetrics().boundingRect(self.btn_start_text).width() + self.btn_offset)
        self.btn_start.clicked.connect(self.start_port_scan)

        # Stop button
        ## Sets stop button text
        self.btn_stop_text = "Stop"
        ## Initializes stop button
        self.btn_stop = QPushButton(self.btn_stop_text)
        self.btn_stop.setMaximumWidth(
            self.btn_stop.fontMetrics().boundingRect(self.btn_stop_text).width() + self.btn_offset)
        self.btn_stop.clicked.connect(self.stop)

        # Input fields for HOST, MAX and MIN port to be scanned
        ## Initializes host field combo box that will be scanned against provided ports
        self.host_field_combobox = QComboBox()
        self.host_field_combobox.setMaximumWidth(self.btn_start.width() * 2.5)
        self.host_field_combobox.setEditable(True)
        self.populate_host_fields()


        ## Min port placegholder set to 1
        self.min_port_placeholder = 1
        ## Text field that holds starting port
        self.min_text_field = QLineEdit()
        self.min_text_field.setMaximumWidth(self.btn_offset)
        self.min_text_field.setMinimumWidth(self.btn_offset)
        self.min_text_field.setText(str(self.min_port_placeholder))

        ## Placeholder for max port set to 65535
        self.max_port_placeholder = 65535
        ## Text field that holds ending port (including)
        self.max_text_field = QLineEdit()
        self.max_text_field.setMaximumWidth(self.btn_offset)
        self.max_text_field.setMinimumWidth(self.btn_offset)
        self.max_text_field.setText(str(self.max_port_placeholder))

        # Text field for Open ports
        ## Text area that displays only open ports
        self.open_ports_text_area = QPlainTextEdit()
        self.open_ports_text_area.setReadOnly(True)

        # Text area for FEEDBACK
        ## Feedback text area
        self.feedback_text_area = QPlainTextEdit()
        self.feedback_text_area.setReadOnly(True)

        # Table for all of the checked ports within given range
        ## Initializes table that will hold all checked ports
        self.port_table = QTableWidget()
        self.port_table.setColumnCount(2)
        self.port_table.setRowCount(0)
        self.port_table.setHorizontalHeaderLabels(["Port", "Status"])

        # Table for all of the checked ports within given range
        ## Initializes table that will hold only opened ports
        self.open_port_table = QTableWidget()
        self.open_port_table.setColumnCount(2)
        self.open_port_table.setRowCount(0)
        self.open_port_table.setHorizontalHeaderLabels(["Port", "Status"])

        # Layout manager
        self.layout = QGridLayout()
        self.layout.addWidget(self.btn_start, 0, 0, 1, 1)
        self.layout.addWidget(self.btn_stop, 0, 1, 1, 1)
        self.layout.addWidget(self.host_field_combobox, 0, 2, 1, 1)
        self.layout.addWidget(self.min_text_field, 0, 3, 1, 1)
        self.layout.addWidget(self.max_text_field, 0, 4, 1, 6)
        self.layout.addWidget(self.open_port_table, 1, 0, 1, 6)
        self.layout.addWidget(self.feedback_text_area, 2, 0, 2, 6)
        self.layout.addWidget(self.port_table, 1, 6, 3, 5)

        # Variables
        ## Iniotializes port scan thread
        self.port_scan_thread = None # will be used later for port scan
        ## Initializes switch if host is alive set to False
        self.is_host_alive = False
        ## Initializes ip
        self.ip = ""
        ## Initializes max port
        self.max_port_available = 65535
        ## Initializes bytes in hex for syn ack message
        self.syn_ack = 0x12
        ## Initializes rst ack first bits in hex (not used for now)
        self.rst_ack = 0x14
        ## Initialized switch that will decide if scan is running
        self.is_stopped = False

        self.port_scan_tab.setLayout(self.layout)
        tabs.addTab(self.port_scan_tab, "Port Scan")

    def populate_host_fields(self):
        """!
            Populates gathered data from database.
            - Initializes controller
            - Retrieves data (if any)
            - Adds each data item gathered
        """
        db = Controller()

        data = db.select_port_scan_items()
        self.host_field_combobox.addItem("")
        for a, value in data:
            self.host_field_combobox.addItem(value)

    def update_host_field(self):
        """!
            Inserts NOT present item to database
            - Checks if item is NOT present within database (no duplicates allowed)
            - If present returns
            - If not present inserts item to database
            - Adds item to dropdown box
        """
        db = Controller()

        if db.is_port_scan_item_present(self.host_field_combobox.currentText()):
            return
        self.host_field_combobox.addItem(self.host_field_combobox.currentText())
        db.insert_port_scan_query(self.host_field_combobox.currentText())

    def stop(self):
        """!
            Stops port scan. Activated when stop button is pressed.
            - Checks if application is running
            - If running switches boolean
            - Else do nothing and terminates function
        """
        if not self.is_stopped:
            self.is_stopped = True
        pass

    def scan_range(self, port_range):
        """!
            Loops through ports, activated when thread is created.
            - Loops through ports
            - Displays ports that were checked
            - Displays active ports

        """
        for port in port_range:
            if self.is_stopped:
                self.is_stopped = False
                break
            is_port_active = self.scan(port)
            row_pos = self.port_table.rowCount()
            self.port_table.insertRow(row_pos)
            self.port_table.setItem(row_pos, 0, QTableWidgetItem(str(port)))
            if is_port_active:
                row_open_pos = self.open_port_table.rowCount()
                self.open_port_table.insertRow(row_open_pos)
                self.open_port_table.setItem(row_open_pos, 0, QTableWidgetItem(str(port)))
                self.open_port_table.setItem(row_open_pos, 1, QTableWidgetItem("Open"))
                self.port_table.setItem(row_pos, 1, QTableWidgetItem("Open"))
            else:
                self.port_table.setItem(row_pos, 1, QTableWidgetItem("Closed"))

    def start_port_scan(self, x=""):
        """!
            Checks if all conditions met before starting port scan thread. Activated when start button is pressed.
            - Checks if host is Alive
            - Checks if valid range is provided
            - Creates range of ports
            - Creates thread
            - Starts thread
            - If some condition is not met provides feedback
        """
        self.clear_table()
        self.ip = self.host_field_combobox.currentText()
        self.update_host_field()
        self.check_host_status()
        if self.is_host_alive is True:
            self.add_feedback("Host " + self.ip + " is UP")
            if self.min_text_field.text().isnumeric() and self.max_text_field.text():
                min_range = int(self.min_text_field.text())
                max_range = int(self.max_text_field.text())
                if 0 <= min_range <= max_range <= self.max_port_available and max_range >= 0:
                    port_range = range(min_range, max_range+1)
                    self.port_scan_thread = threading.Thread(target=self.scan_range, args=(port_range,))
                    self.port_scan_thread.start()
                else:
                    self.add_feedback("invalid range specified")
            else:
                self.add_feedback("Invalid range values specified")
        else:
            self.add_feedback("Host " + self.ip + " is DOWN")

    def check_host_status(self):
        """!
            Checks if host is alive.
            - Sends ICMP to confirm
        """
        self.add_feedback("Checking host at " + self.ip + " .......")
        try:
            icmp_check = sr1(IP(dst=self.ip) / ICMP(), timeout=1)
            if "IP" in icmp_check:
                self.is_host_alive = True
        except Exception:
            self.is_host_alive = False

    def scan(self, dst_port):
        """!
            Check if port is opened.
            - Creates random port number as source port
            - Sends SYN ACK packet

            @param dst_port  destination port
            @return True on success, otherwise False
        """
        try:
            src_port = RandShort()
            syn_ack_pkt = sr1(IP(dst=self.ip)/TCP(sport=src_port, dport=dst_port, flags="S"), timeout=0.1)
            pkt_flags = syn_ack_pkt.getlayer(TCP).flags
            return True
        except AttributeError:
            return False

    def clear_table(self):
        """!
            Clears table network capture table entries. Activated when clear button pressed.
            - Appends text to feedback field indicating of active network capture
            - If table is empty terminates function
            - If NOT empty:
            - Erases details field sets to None
            - Sets table row counts to 0 (erasing all entries)
        """
        if self.port_table.rowCount() != 0:
            self.add_feedback("Clearing out previous results")
            self.port_table.setRowCount(0)

    def add_feedback(self, feedback_text):
        """!
            Appends feedback to user of current activity

            @param feedback_text  String that will be displayed within feedback field
        """
        self.feedback_text_area.appendPlainText("[+] " + feedback_text)
