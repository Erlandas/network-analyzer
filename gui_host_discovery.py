"""!
    @brief Defines a HostDiscovery class
"""
# TODO exception handling for invalid host and subnet
# TODO perform host scan in threads
# TODO include different options for scan (ICMP, TCP, silent, ARP)
##
# @file gui_main_window.py
#
# @brief Defines a MainWindow class
#
# @section description_gui_main_window Description
# Defines base class for graphical user interface components
# - gui_main_window (base class)
#
# @section libraries_gui_main_window Libraries/Modules
# - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
#   - Access to QWidget
#   - Access to QGridLayout
#   - Access to QPushButton
#   - Access to QPlainTextEdit
#   - Access to QTableWidget
#   - Access to QTableWidgetItem
#   - Access to QComboBox
# - db_controls
#   - Access to Controller class
# - threading
#   - Access to SpoofDns class
# - gui_port_scan
#   - Access to PortScan class
# - gui_host_discovery
#   - Access to HostDiscovery class
# - gui_arp_poison
#   - Access to ArpPoison class
# - gui_quick_note
#   - Access to QuickNote class
#
# @section notes_gui_main_window Notes
# - None
#
# @section todo_gui_main_window TODO
# - None
#
# @section author_gui_main_window Author(s)
# - Created by Erlandas Bacauskas on 2021/2022
#
# Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
# To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/

import threading

import scapy.all as scapy
from PyQt5.QtWidgets import (
    QWidget,
    QGridLayout,
    QPushButton,
    QPlainTextEdit,
    QTableWidget,
    QTableWidgetItem,
    QComboBox
)
from db_controls import Controller


class HostDiscovery(QWidget):
    """!
        @brief Defines a HostDiscovery class

        @section description_sniffer Description
        The HostDiscovery class is responsible for all components that is capable to identify hosts on a network
        and to displaying result to user.

        @section libraries_gui_sniffer Libraries/Modules
        - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
          - Access to QWidget
          - Access to QGridLayout
          - Access to QPushButton
          - Access to QComboBox
          - Access to QTableWidget
          - Access to QPlainTextEdit
          - Access to QTableWidgetItem
        - Scapy https://scapy.readthedocs.io/en/latest/
          - Access to scapy functionality
        - db_controls
          - Access to Controller class

        @section todo_gui_main_window TODO
        - Implement ICMP scan type

        @section author_gui_sniffer Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
    """

    def __init__(self, tabs):
        """!
            The HostDiscovery class initializer.
            @return Tab for a Host Discovery initialized with components
        """
        super().__init__()
        ## Creates tab that will be returned
        self.host_discovery_tab = QWidget()


        # Components
        # Start button
        ## Sets offset for buttons
        self.btn_offset = 100
        ## Sets start button text
        self.btn_start_text = "Start"
        ## Initializes start button
        self.btn_start = QPushButton(self.btn_start_text)
        self.btn_start.setMaximumWidth(
            self.btn_start.fontMetrics().boundingRect(self.btn_start_text).width() + self.btn_offset)
        self.btn_start.clicked.connect(self.start_host_discovery)

        ###############################################
        ## Initializes dropdown combo box for subnet/IP to check
        self.ip_or_subnet_field_combobox = QComboBox()
        self.ip_or_subnet_field_combobox.setMaximumWidth(self.btn_start.width()*2.5)
        self.ip_or_subnet_field_combobox.setEditable(True)
        self.populate_host_discovery_queries()
        ###############################################

        # Text Area for host selected information
        ## Initializes text area where active hosts will be displayed
        self.active_hosts_text_area = QPlainTextEdit()
        self.active_hosts_text_area.setReadOnly(True)

        # Table for hosts
        ## Initializes table where results will be displayed of active hosts
        self.host_table = QTableWidget()
        self.host_table.setColumnCount(3)
        self.host_table.setRowCount(0)
        self.host_table.setHorizontalHeaderLabels(["IP", "MAC", "Status"])
        self.host_table.clicked.connect(self.table_row_item_selected)

        # Drop box for host scan type
        ## Initialized scan type to be chosen for host discovery
        # TODO implement ICMP scan
        self.scan_type_dropbox = QComboBox()
        for scan_type in list(["ARP", "ICMP - not implemented"]):
            self.scan_type_dropbox.addItem(scan_type)
        self.scan_type_dropbox.setMaximumWidth(200)
        #############
        # self.scan_type_dropbox.setEditable(True)
        #############
        self.scan_type_dropbox.activated.connect(self.set_scan_type)

        # Variables
        ## Sets String with default scan option (ARP)
        self.scan_type_option = "ARP"
        ## Holts host information
        self.clients = None
        ## Creates thread for host discovery
        self.host_discovery_thread = None

        # Layout manager
        self.layout = QGridLayout()
        self.layout.addWidget(self.btn_start, 0, 0, 1, 1)
        self.layout.addWidget(self.ip_or_subnet_field_combobox, 0, 1, 1, 1)
        self.layout.addWidget(self.scan_type_dropbox, 0, 2, 1, 6)
        self.layout.addWidget(self.host_table, 1, 0, 1, 4)
        self.layout.addWidget(self.active_hosts_text_area, 1, 4, 1, 4)
        self.host_discovery_tab.setLayout(self.layout)
        tabs.addTab(self.host_discovery_tab, "Host Discovery")

    def populate_host_discovery_queries(self):
        """!
            Populates gathered data from database in ip or subnet dropdown field.
            - Initializes controller
            - Retrieves data (if any)
            - Adds each data item gathered
        """
        db = Controller()
        data = db.select_host_discovery_items()
        self.ip_or_subnet_field_combobox.addItem("")
        for a, value in data:
            self.ip_or_subnet_field_combobox.addItem(value)

    def update_host_discovery_queries(self):
        """!
            Inserts NOT present item to database (Host discovery)
            - Checks if item is NOT present within database (no duplicates allowed)
            - If present returns
            - If not present inserts item to database
            - Adds item to filter dropdown box
        """
        db = Controller()

        if db.is_host_discovery_item_present(self.ip_or_subnet_field_combobox.currentText()):
            return
        db.insert_host_discovery_query(self.ip_or_subnet_field_combobox.currentText())
        self.ip_or_subnet_field_combobox.addItem(self.ip_or_subnet_field_combobox.currentText())

    def set_scan_type(self):
        """!
            Sets scan type from dropdown box. Default is ARP.
        """
        self.scan_type_option = self.scan_type_dropbox.currentText()

    def start_host_discovery(self):
        """!
            Starts host discovery. Activated when start button is pressed.
            - Determines scan type
            - Initializes thread for host discovery
            - Starts thread
        """
        self.active_hosts_text_area.setPlainText("")
        if self.scan_type_option == "ARP":
            self.host_discovery_thread = threading.Thread(target=self.arp_scan, args=(1,))
            self.host_discovery_thread.start()
        else:
            pass

    def table_row_item_selected(self):
        """!
            Populates text field that displays information about selected table item.
        """
        self.active_hosts_text_area.setPlainText("[+] Request")
        self.active_hosts_text_area.appendPlainText(self.clients[self.host_table.currentRow()][0].show(dump=True))
        self.active_hosts_text_area.appendPlainText("[+] Response")
        self.active_hosts_text_area.appendPlainText(self.clients[self.host_table.currentRow()][1].show(dump=True))

    def arp_scan(self, x):
        """!
            Performs ARP scan by checking target(s) from provided IP/Subnet.
            - Sets current client/host to None
            - Creates broadcast ARP packet
            - Checks for host
            - If host ALIVE entry added to table
        """
        try:
            self.clients = None
            self.host_table.setRowCount(0)
            request = scapy.ARP()
            request.pdst = self.ip_or_subnet_field_combobox.currentText()
            broadcast = scapy.Ether()
            broadcast.dst = 'ff:ff:ff:ff:ff:ff'
            request_broadcast = broadcast / request
            self.clients = scapy.srp(request_broadcast, timeout=1)[0]
            for element in self.clients:
                row_pos = self.host_table.rowCount()
                self.host_table.insertRow(row_pos)
                self.host_table.setItem(row_pos, 0, QTableWidgetItem(element[1].psrc))
                self.host_table.setItem(row_pos, 1, QTableWidgetItem(element[1].hwsrc))
                self.host_table.setItem(row_pos, 2, QTableWidgetItem("ACTIVE"))
            self.update_host_discovery_queries()
        except:
            pass