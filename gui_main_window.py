"""! @brief Defines a MainWindow class."""

from PyQt5.QtWidgets import (
    QWidget,
    QFrame,
    QVBoxLayout,
    QDesktopWidget,
    QTabWidget,
)

from gui_sniffer import Sniffer
from gui_port_scan import PortScan
from gui_host_discovery import HostDiscovery
from gui_arp_poison import ArpPoison
from gui_dhcp_starvation import DHCStarvation
from gui_dos import DoS


class MainWindow(QWidget):
    """!
        @brief Defines a MainWindow class

        @section description_gui_main_window Description
        The MainWindow class that layouts components and provides access to these components of an application.
        Defines all components and layouts them.
        - gui_main_window (base class)

        @section libraries_gui_main_window Libraries/Modules
        - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
          - Access to QWidget
          - Access to QFrame
          - Access to QVBoxLayout
          - Access to QDesktopWidget
          - Access to QTabWidget
        - gui_sniffer
          - Access to Sniffer class
        - gui_dns_spoofer
          - Access to SpoofDns class
        - gui_port_scan
          - Access to PortScan class
        - gui_host_discovery
          - Access to HostDiscovery class
        - gui_arp_poison
          - Access to ArpPoison class
        - gui_quick_note
          - Access to QuickNote class

        @section author_gui_main_window Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
    """

    def __init__(self):
        """!
            The MainWindow class initializer.
            @return Application window initialized with components
        """

        super().__init__()
        ## Initializes frame
        self.frame = QFrame()
        ## Creates layout associated with QVBoxLayout
        self.layout = QVBoxLayout()
        self.frame.setLayout(self.layout)
        self.setWindowTitle("Network Analyzer")
        ## Gets current active screen resolution
        self.screen_resolution = QDesktopWidget().screenGeometry()
        self.setGeometry(
            self.screen_resolution.width() / 2 - self.screen_resolution.width() / 4,
            self.screen_resolution.height() / 2 - self.screen_resolution.height() / 4,
            self.screen_resolution.width() / 2,
            self.screen_resolution.height() / 2
        )
        ## Creates container that holds tabs
        self.tabs = QTabWidget()
        ## Creates/Initializes sniffer tab
        self.sniff_tab = Sniffer(self.tabs)
        ## Creates/Initializes host discovery tab
        self.host_discovery_tab = HostDiscovery(self.tabs)
        ## Creates/Initializes port scanner tab
        self.port_scan_tab = PortScan(self.tabs)
        ## Creates/Initializes ARP poisoning tab
        self.arp_poison_tab = ArpPoison(self.tabs)
        ## Creates/Initializes DNS spoof tab
        ##self.dns_spoof_tab = SpoofDns(self.tabs)
        ## Creates/Initializes DHCP Starvation tab
        self.quick_note_tab = DHCStarvation(self.tabs)
        ## Creates/Initializes DoS tab
        self.dos_tab = DoS(self.tabs)
        ## sets layout of components
        self.setLayout(self.layout)
        ## Adds tabs to create tabular design
        self.layout.addWidget(self.tabs)
        self.show()
