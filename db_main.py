"""! @brief Defines a Data class."""

## Examples
# @file db_main.py
#
# @brief Defines a Data class
#
# @section description_db_main Description
# Defines base class for database usage
# - Data
#
# @section libraries_db_main Libraries/Modules
# - sqlite3 https://docs.python.org/3/library/sqlite3.html
#
# @section notes_db_main Notes
# - None
#
# @section todo_db_main TODO
# - None
#
# @section author_db_main Author(s)
# - Created by Erlandas Bacauskas on 2021/2022
#
# Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
# To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/


import sqlite3
from sqlite3 import Error


class Data:
    """!
        @brief Defines a Data class

        @section description_db_main Description
        The Data class.
        Defines class that is responsible to directly communicate with database.

        @section libraries_db_main Libraries/Modules
        - sqlite3 https://docs.python.org/3/library/sqlite3.html

        @section author_db_main Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
    """

    def __init__(self):
        """!
            The Data class initializer.
            @return An instance of the Data class.
        """
        ## Defines connection with database
        self.conn = None
        sql_port_scan = """ CREATE TABLE IF NOT EXISTS port_scan (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            query text NOT NULL
                                        ); """
        sql_host_discovery = """ CREATE TABLE IF NOT EXISTS host_discovery (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            query text NOT NULL
                                        ); """
        sql_filter_options = """ CREATE TABLE IF NOT EXISTS filter_options (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            query text NOT NULL
                                        ); """
        sql_arp_poison_targets = """ CREATE TABLE IF NOT EXISTS arp_targets (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            query text NOT NULL
                                        ); """
        sql_arp_poison_gateways = """ CREATE TABLE IF NOT EXISTS arp_gateways (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            query text NOT NULL
                                        ); """
        sql_dns_spoof_domains = """ CREATE TABLE IF NOT EXISTS dns_spoof_domain (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            query text NOT NULL
                                        ); """
        sql_dns_spoof_ip = """ CREATE TABLE IF NOT EXISTS dns_spoof_ip (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            query text NOT NULL
                                        ); """
        sql_dos_targets = """ CREATE TABLE IF NOT EXISTS dos_targets (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            query text NOT NULL
                                        ); """

        ## Sets database name
        self.db_name = "app_data.db"
        self.create_connection()
        self.create_table(sql_port_scan)
        self.create_table(sql_host_discovery)
        self.create_table(sql_filter_options)
        self.create_table(sql_arp_poison_targets)
        self.create_table(sql_arp_poison_gateways)
        self.create_table(sql_dns_spoof_domains)
        self.create_table(sql_dns_spoof_ip)
        self.create_table(sql_dos_targets)
        self.close_connection()

    def create_connection(self):
        """!
            Creates connection with a database used for application.
        """
        try:
            self.conn = sqlite3.connect(self.db_name)
        except Error as e:
            pass

    def close_connection(self):
        """!
            Closes connection between database and application
        """
        if self.conn:
            self.conn.close()

    def create_table(self, sql):
        """!
            Creates table within database if table does not exist.

            @param sql  sql statement that must be executed in order to create table
        """
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql)
        except Error as e:
            pass

    def insert_into_table(self, sql, data):
        """!
            Inserts data/item/value into table defined in the sql string

            @param sql  sql statement that must be executed
            @param data  data that must be inserted into a table defined in sql statement
        """
        cursor = self.conn.cursor()
        cursor.execute(sql, data)
        self.conn.commit()

    def select_items(self, sql):
        """!
            Select data/item/value from table defined in the sql string

            @param sql  sql statement that must be executed
            @return dataset retrieved using given sql statement
        """
        cursor = self.conn.cursor()
        cursor.execute(sql)
        return cursor.fetchall()

    def select_item(self, sql):
        """!
            Select data/item/value from table defined in the sql string

            @param sql  sql statement that must be executed
            @return data/item/value retrieved using given sql statement
        """
        cursor = self.conn.cursor()
        cursor.execute(sql)
        return cursor.fetchall()
