"""! @brief Defines a Controller class."""

## EXAMPLE TO BE DELETED ----------------------------------------------------------
# @file db_controls.py
#
# @brief Defines a Controller class
#
# @section description_db_controls Description
# Defines base class for database usage
# - Controller
#
# @section libraries_gui_main_window Libraries/Modules
# - db_main
#   - Access to Data class
#
# @section notes_db_controls Notes
# - None
#
# @section todo_db_controls TODO
# - None
#
# @section author_db_controls Author(s)
# - Created by Erlandas Bacauskas on 2021/2022
#
# Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
# To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/

from db_main import Data


class Controller:
    """!
        @brief Defines a Controller class

        @section description_db_controls Description
        The Controller class.
        Provides access from graphical user interface to underlying database.

        @section libraries_gui_main_window Libraries/Modules
        - db_main
          - Access to Data class

        @section author_db_controls Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
    """

    def __init__(self):
        """!
            The Controller class initializer.
            @return instance of controller class that enables communication between graphical user interface and database
        """
        ## Initializes database for data handling
        self.db = Data()

    # HOST DISCOVERY RELATED
    def insert_host_discovery_query(self, value):
        """!
            Host discovery related:
            Inserts given value into host discovery table

            @param value  Value that will be inserted into host_discovery table
        """
        self.db.create_connection()
        sql = ''' INSERT INTO host_discovery(query)
                              VALUES(?) '''
        self.db.insert_into_table(sql, [value])
        self.db.close_connection()

    def select_host_discovery_items(self):
        """!
            Host discovery related:
            Selects host discovery items that are present within host discovery table

            @return A collection of data that was selected from host discovery table
        """
        self.db.create_connection()
        sql = ''' SELECT * FROM host_discovery '''
        data = self.db.select_items(sql)
        self.db.close_connection()
        return data

    def is_host_discovery_item_present(self, item):
        """!
            Host discovery related:
            Checks if given item is present within database

            @param item  Item that must be checked against dataset within database
            @return If item is present returns True otherwise returns False
        """
        self.db.create_connection()
        sql = f''' SELECT * FROM host_discovery WHERE query="{item}"'''
        data = self.db.select_item(sql)
        self.db.close_connection()
        if data:
            return True
        return False

    # PORT SCAN RELATED
    def insert_port_scan_query(self, value):
        """!
            Port scanner related:
            Inserts given value into port scan table

            @param value  Value that will be inserted into port scan table
        """
        self.db.create_connection()
        sql = ''' INSERT INTO port_scan(query)
                              VALUES(?) '''
        self.db.insert_into_table(sql, [value])
        self.db.close_connection()

    def select_port_scan_items(self):
        """!
            Port scanner related:
            Selects port scan items that are present within port scan table

            @return A collection of data that was selected from port scan table

        """
        self.db.create_connection()
        sql = ''' SELECT * FROM port_scan '''
        data = self.db.select_items(sql)
        self.db.close_connection()
        return data

    def is_port_scan_item_present(self, item):
        """!
            Port scanner related:
            Checks if given item is present within database

            @param item  Item that must be checked against dataset within database
            @return If item is present returns True otherwise returns False

        """
        self.db.create_connection()
        sql = f''' SELECT * FROM port_scan WHERE query="{item}"'''
        data = self.db.select_item(sql)
        self.db.close_connection()
        if data:
            return True
        return False

    # SNIFFER RELATED

    def insert_filter_option(self, value):
        """!
            Network sniffer related:
            Inserts given value into filter options table

            @param value  Value that will be inserted into filter options table
        """
        self.db.create_connection()
        sql = ''' INSERT INTO filter_options(query)
                              VALUES(?) '''
        self.db.insert_into_table(sql, [value])
        self.db.close_connection()

    def select_filter_option_items(self):
        """!
            Network sniffer related:
            Selects filter options items that are present within filter options table

            @return A collection of data that was selected from filter options table

        """
        self.db.create_connection()
        sql = ''' SELECT * FROM filter_options '''
        data = self.db.select_items(sql)
        self.db.close_connection()
        return data

    def is_filter_option_item_present(self, item):
        """!
            Network sniffer related:
            Checks if given item is present within database

            @param item  Item that must be checked against dataset within database
            @return If item is present returns True otherwise returns False

        """
        self.db.create_connection()
        sql = f''' SELECT * FROM filter_options WHERE query="{item}"'''
        data = self.db.select_item(sql)
        self.db.close_connection()
        if data:
            return True
        return False

    # ARP POISONING RELATED

    def insert_arp_target(self, value):
        """!
            ARP poisoning related (ARP targets):
            Inserts given value into arp targets table

            @param value  Value that will be inserted into arp targets table
        """
        self.db.create_connection()
        sql = ''' INSERT INTO arp_targets(query)
                              VALUES(?) '''
        self.db.insert_into_table(sql, [value])
        self.db.close_connection()

    def select_arp_target_items(self):
        """!
            ARP poisoning related (ARP targets):
            Selects host discovery items that are present within arp targets table

            @return A collection of data that was selected from arp targets table

        """
        self.db.create_connection()
        sql = ''' SELECT * FROM arp_targets '''
        data = self.db.select_items(sql)
        self.db.close_connection()
        return data

    def is_arp_target_present(self, item):
        """!
            ARP poisoning related (ARP targets):
            Checks if given item is present within database

            @param item  Item that must be checked against dataset within database
            @return If item is present returns True otherwise returns False

        """
        self.db.create_connection()
        sql = f''' SELECT * FROM arp_targets WHERE query="{item}"'''
        data = self.db.select_item(sql)
        self.db.close_connection()
        if data:
            return True
        return False

    def insert_arp_gateway(self, value):
        """!
            ARP poisoning related (ARP gateways):
            Inserts given value into arp gateways table

            @param value  Value that will be inserted into arp gateways table
        """
        self.db.create_connection()
        sql = ''' INSERT INTO arp_gateways(query)
                              VALUES(?) '''
        self.db.insert_into_table(sql, [value])
        self.db.close_connection()

    def select_arp_gateway_items(self):
        """!
            ARP poisoning related (ARP gateways):
            Selects host discovery items that are present within arp gateways table

            @return A collection of data that was selected from arp gateways table

        """
        self.db.create_connection()
        sql = ''' SELECT * FROM arp_gateways '''
        data = self.db.select_items(sql)
        self.db.close_connection()
        return data

    def is_arp_gateway_present(self, item):
        """!
            ARP poisoning related (ARP gateways):
            Checks if given item is present within database

            @param item  Item that must be checked against dataset within database
            @return If item is present returns True otherwise returns False

        """
        self.db.create_connection()
        sql = f''' SELECT * FROM arp_gateways WHERE query="{item}"'''
        data = self.db.select_item(sql)
        self.db.close_connection()
        if data:
            return True
        return False

    # DNS SPOOF RELATED

    def insert_dns_spoof_domain(self, value):
        """!
            DNS Spoof related (domain names):
            Inserts given value into dns spoof domains table

            @param value  Value that will be inserted into dns spoof domains table
        """
        self.db.create_connection()
        sql = ''' INSERT INTO dns_spoof_domain(query)
                              VALUES(?) '''
        self.db.insert_into_table(sql, [value])
        self.db.close_connection()

    def select_dns_spoof_domains(self):
        """!
            DNS Spoof related (domain names):
            Selects dns spoof domain items that are present within dns spoof domains table

            @return A collection of data that was selected from dns spoof domains table

        """
        self.db.create_connection()
        sql = ''' SELECT * FROM dns_spoof_domain '''
        data = self.db.select_items(sql)
        self.db.close_connection()
        return data

    def is_dns_spoof_domain_present(self, item):
        """!
            DNS Spoof related (domain names):
            Checks if given item is present within database

            @param item  Item that must be checked against dataset within database
            @return If item is present returns True otherwise returns False

        """
        self.db.create_connection()
        sql = f''' SELECT * FROM dns_spoof_domain WHERE query="{item}"'''
        data = self.db.select_item(sql)
        self.db.close_connection()
        if data:
            return True
        return False

    def insert_dns_spoof_ip(self, value):
        """!
            DNS Spoof related (ips):
            Inserts given value into dns spoof domains table

            @param value  Value that will be inserted into dns spoof ips table
        """
        self.db.create_connection()
        sql = ''' INSERT INTO dns_spoof_ip(query)
                              VALUES(?) '''
        self.db.insert_into_table(sql, [value])
        self.db.close_connection()

    def select_dns_spoof_ip(self):
        """!
            DNS Spoof related (ip):
            Selects dns spoof domain items that are present within dns spoof domains table

            @return A collection of data that was selected from dns spoof ips table

        """
        self.db.create_connection()
        sql = ''' SELECT * FROM dns_spoof_ip '''
        data = self.db.select_items(sql)
        self.db.close_connection()
        return data

    def is_dns_spoof_ip_present(self, item):
        """!
            DNS Spoof related (ip):
            Checks if given item is present within database

            @param item  Item that must be checked against dataset within database
            @return If item is present returns True otherwise returns False

        """
        self.db.create_connection()
        sql = f''' SELECT * FROM dns_spoof_ip WHERE query="{item}"'''
        data = self.db.select_item(sql)
        self.db.close_connection()
        if data:
            return True
        return False

    # DOS RELATED
    def insert_dos_query(self, value):
        """!
            DoS related:
            Inserts given value into DoS targets table

            @param value  Value that will be inserted into DoS targets table
        """
        self.db.create_connection()
        sql = ''' INSERT INTO dos_targets(query)
                              VALUES(?) '''
        self.db.insert_into_table(sql, [value])
        self.db.close_connection()

    def select_dos_items(self):
        """!
            DoS related:
            Selects DoS items that are present within DoS targets table

            @return A collection of data that was selected from DoS targets table

        """
        self.db.create_connection()
        sql = ''' SELECT * FROM dos_targets '''
        data = self.db.select_items(sql)
        self.db.close_connection()
        return data

    def is_dos_item_present(self, item):
        """!
            DoS related:
            Checks if given item is present within database

            @param item  Item that must be checked against dataset within database
            @return If item is present returns True otherwise returns False

        """
        self.db.create_connection()
        sql = f''' SELECT * FROM dos_targets WHERE query="{item}"'''
        data = self.db.select_item(sql)
        self.db.close_connection()
        if data:
            return True
        return False
